const User = require('./../models/User.js')
const Product = require('./../models/Product.js')
const bcrypt = require("bcrypt");
const auth = require("./../auth.js");

module.exports.checkEmail = (reqBody) => {
	return User.findOne( {email: reqBody.email}).then( (result, error) => {
		
		if(result != null) {
			return `Email already exists`
		} else {
			if(result == null) {
				return true
			} else {
				return error
			}
		}
	})
}

module.exports.register = (reqBody) => {
	return User.findOne( {email: reqBody.email}).then( (result, error) => {
		if(result != null) {
			return `Email exists`
		} else {
			let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})
			return newUser.save().then( (result, error) => {
				if(result){
					return true
				} else {
					return error
				}
			})
		}
	})
}

module.exports.getAllUsers = () => {
	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

module.exports.login = (reqBody) => {
	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {
			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.setUserAsAdmin = (data) => {
	const {id} = data

	return User.findByIdAndUpdate(id, {isAdmin: true}).then((result, err) => {
		if(result != null){
			result.password = "";
			return result
		} else {
			return false
		}
	})
}

module.exports.createProduct = (data, reqBody) => {
	const {id} = data

	return User.findById(id).then( (result, error) => {
		if(result.isAdmin != false){

			let newProduct = new Product({
				"name": reqBody.name,
				"description": reqBody.description,
				"price": reqBody.price
			})

			return newProduct.save().then( (result, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find().then( (result, error) => {
		if(result) {
			return result
		} else {
			return error
		}
	})
}

module.exports.getSingleProduct = (params) => {
	return Product.findById(params).then( (result, error) => {
		if(result == null){
			return `Product is not existing.`
		} else {
			if(result){
				return result
			} else {
				return error
			}
		}
	})
}

module.exports.updateProduct = (data, params, reqBody) => {
	const {id} = data

	return User.findById(id).then( (result, error) => {
		if(result.isAdmin != false) {
			let product = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				isActive: reqBody.isActive
			}

			return Product.findByIdAndUpdate( params, product).then( (result, error) => {

				if(result == null) {
					return `Product is not existing.`
				} else {
					if(result){
						return true
					} else {
						return false
					}
				}
			})
		} else {
			return false
		}
	})
}

module.exports.archiveProduct = (data, params) => {
	const {id} = data

	return User.findById(id).then( (result, error) => {

		if(result.isAdmin != false) {
			return Product.findByIdAndUpdate(params, {isActive: false}).then( (result, error) => {

				if(result == null){
					return `Product is not existing.`
				} else {
					if(result){
						return result
					} else {
						return false
					}
				}
			})

		} else {
			return false
		}
	})
}