const express = require("express");
const router = express.Router();

const userController = require('./../controllers/userControllers.js')
const auth = require('./../auth.js')

//checking of user email
router.post("/email-exists", (req, res) => {
	userController.checkEmail(req.body).then( result => res.send(result))
})

//register user
router.post("/register", (req, res) => {
	userController.register(req.body).then( result => res.send(result))
})

//get all users
router.get("/", (req, res) => {
	userController.getAllUsers().then( result => res.send(result))
})

//login user
router.post("/login", (req, res) => {
	userController.login(req.body).then( result => res.send(result))
})

//set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	userController.setUserAsAdmin(userData).then(result => res.send(result))
})

//create a product (Admin Side)
router.post("/products", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)
	userController.createProduct(userData, req.body).then(result => res.send(result))
})

//get all products
router.get("/products", (req, res) => {
	userController.getAllProducts(req.body).then( result => res.send(result))
})

//get single product by ID
router.get("/products/:productId", (req, res) => {
	let paramsId = req.params.productId
	userController.getSingleProduct(paramsId).then( result => res.send(result))
})

//update product (Admin Side)
router.put("/products/:productId", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)
	userController.updateProduct(userData, req.params.productId, req.body).then(result => res.send(result))
})

//archive product (Admin Side)
router.put("/products/:productId/archive", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)
	userController.archiveProduct(userData, req.params.productId).then(result => res.send(result))
})




module.exports = router;