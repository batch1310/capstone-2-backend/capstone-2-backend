const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	totalAmout: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userOrdered: [
		{
			userId: {
				type: String,
				required: [true,"User ID is required"]
			},
			productOrdered: {
				name: String				
			}
		}
	]

})

module.exports = mongoose.model("Order", orderSchema)